package tw.davidlee.demo.microservice.producer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tw.davidlee.starter.rabbitmq.enums.RabbitMQEnum;
import tw.davidlee.starter.rabbitmq.utils.RabbitMQUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestRabbitMQ {

    @Autowired
    RabbitMQUtil rabbitMQMessageUtil;

    @Test
    public void sendMQ(){
        rabbitMQMessageUtil.sendMessage(RabbitMQEnum.NOTICE,"HELLO THIS IS MQ");
    }


    // list to map
//    Map m = list.stream().collect(Collectors.toMap(TestRabbitMQ::getDeploymentId ,
//            e -> Lists.newArrayList( new TestRabbitMQ(e.getRoleId() ,e.getRoleName())   ),
//            (List<TestRabbitMQ> oldList, List<TestRabbitMQ> newList) -> {
//                oldList.addAll(newList);
//                return oldList;
//            }));
}

