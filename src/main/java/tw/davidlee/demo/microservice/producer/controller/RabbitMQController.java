package tw.davidlee.demo.microservice.producer.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tw.davidlee.demo.microservice.producer.bo.RabbitMQBO;
import tw.davidlee.microservice.springmvc.http.ApiResponse;
import tw.davidlee.starter.rabbitmq.enums.RabbitMQEnum;
import tw.davidlee.starter.rabbitmq.utils.RabbitMQUtil;

@Slf4j
@RestController
@RequestMapping("/api/v1/rabbitMQ")
@Api(tags ="Rabbit MQ 操作")
public class RabbitMQController {


    /**
     *    引用 rabbitMQ Util
     */
    @Autowired
    RabbitMQUtil rabbitMQUtil;

    @ApiResponses(value = {@io.swagger.annotations.ApiResponse(code=200,message = "成功")})
    @ApiOperation( value= "發送訊息至RabbitMQ")
    @PostMapping(value = "/message" , produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse sendRabbitMQ( @RequestBody RabbitMQBO rabbitMQBO) {
        rabbitMQUtil.sendMessage(RabbitMQEnum.NOTICE, rabbitMQBO);
        return new ApiResponse();
    }

}
