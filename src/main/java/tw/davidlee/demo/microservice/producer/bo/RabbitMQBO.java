package tw.davidlee.demo.microservice.producer.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "RabbitMQ 消息生產者")
public class RabbitMQBO {

    @ApiModelProperty(value = "消息內容", required = true)
    String message;
}
