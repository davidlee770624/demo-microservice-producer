package tw.davidlee.demo.microservice.producer.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1/sso")
@Api(tags ="SSO 操作")
public class SsoController {
    @GetMapping("/normal")
    @PreAuthorize("hasAuthority('ROLE_NORMAL')")
    @ApiOperation( value= "ROLE_NORMAL權限")
    public String normal( ) {
        return "normal permission test success !!!";
    }

    @GetMapping("/medium")
    @PreAuthorize("hasAuthority('ROLE_MEDIUM')")
    @ApiOperation( value= "ROLE_MEDIUM權限")
    public String medium() {
        return "medium permission test success !!!";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @ApiOperation( value= "ROLE_ADMIN權限")
    public String admin() {
        return "admin permission test success !!!";
    }
}
